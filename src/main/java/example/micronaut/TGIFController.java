package example.micronaut;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("tgif")
public class TGIFController {

    @Get("/")
    public String index() {
        return "tgif";
    }

}
