package example.micronaut;

import javax.inject.Singleton;

@Singleton
public class ExampleService {

    public ExampleService() {
    }

    public String getSunday() {
        return "Sunday";
    }


    public String getFriday() {
        return "Friday";
    }
}
