package example.micronaut.common;

import cucumber.api.event.*;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;

import java.io.File;

public class Initialization implements EventListener {

    public static DockerComposeContainer environment;
    public static int appInternalPort = 8080;
    public static String appInstanceName = "micronaut-app_1";
    public static int appExternalPort;
    public static String appExternalHost;


    private EventHandler<TestRunStarted> setup = event -> {

        System.out.println();
        System.out.println("******************************");
        System.out.println("***** TEST CONTEXT SETUP *****");
        System.out.println("******************************");
        System.out.println();

        System.out.println();
        System.out.println("Configuring Docker environment ...");
        System.out.println();

        environment = new DockerComposeContainer(new File("src/test/resources/docker-compose.yml"))
                .withExposedService(appInstanceName, appInternalPort, Wait.forHttp("/hello")
                        .forStatusCode(200)
                ).withLocalCompose(true);

        System.out.println();
        System.out.println("f1-c1");
        System.out.println("f1-c2");
        System.out.println("m1-c1");
        System.out.println("m2-c1");
        System.out.println("Starting Docker environment ...");
        System.out.println();

        environment.start();

        System.out.println();
        System.out.println("Docker environment started ...");
        System.out.println();

        appExternalHost = Initialization.environment.getServiceHost(Initialization.appInstanceName, appInternalPort);
        appExternalPort = Initialization.environment.getServicePort(Initialization.appInstanceName, appInternalPort);

        System.out.println();
        System.out.println("Application URL = " + appExternalHost + ":" + appExternalPort);
        System.out.println();

    };

    private EventHandler<TestRunFinished> teardown = event -> {
        System.out.println();
        System.out.println("*********************************");
        System.out.println("***** TEST CONTEXT TEARDOWN *****");
        System.out.println("*********************************");
        System.out.println();


        environment.stop();
        System.out.println();
        System.out.println("Docker environment stopped ...");
        System.out.println();


    };

    @Override
    public void setEventPublisher(EventPublisher publisher) {
        publisher.registerHandlerFor(TestRunStarted.class, setup);
        publisher.registerHandlerFor(TestRunFinished.class, teardown);
    }
}