package example.micronaut.steps;

import example.micronaut.ExampleService;
import example.micronaut.common.Initialization;
import example.micronaut.utils.IsItFridayYetFeatureUtils;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.Assertions;

import java.io.IOException;

public class DaysSteps {

    private String today;
    private String actualAnswer;
    private ExampleService exampleService;


    //DI example with PicoContainer
    public DaysSteps(ExampleService exampleService) {
        this.exampleService = exampleService;
        Assertions.assertNotNull(this.exampleService, "ExampleService should not be null!");

    }

    @Given("today is Sunday")
    public void today_is_Sunday() {
        today = this.exampleService.getSunday();
    }

    @Given("today is Friday")
    public void today_is_Friday() {
        // Write code here that turns the phrase above into concrete actions
        today = this.exampleService.getFriday();
    }

    @When("I ask whether it's Friday yet")
    public void i_ask_whether_it_s_Friday_yet() {
        actualAnswer = IsItFridayYetFeatureUtils.isItFriday(today);
    }

    @Then("I should be told {string}")
    public void i_should_be_told(String expectedAnswer) throws IOException {

        CloseableHttpClient httpClient = HttpClients.createDefault();
        String URL = "http://" + Initialization.appExternalHost + ":" + Initialization.appExternalPort + "/" + expectedAnswer;
        HttpGet get = new HttpGet(URL);
        // StringEntity entity = new StringEntity(jsonString);
        get.addHeader("content-type", "application/json");
        // request.setEntity(entity);
        HttpResponse response = httpClient.execute(get);

        String s = IOUtils.toString(response.getEntity().getContent());

        Assertions.assertEquals(expectedAnswer, s);


        // assertEquals(expectedAnswer, actualAnswer);
    }
}
