package example.micronaut;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "example.micronaut.common.Initialization"},
        features = "src/test/resources/example/micronaut"
)
public class RunCucumberTest {


}
