package example.micronaut.utils;

public final class IsItFridayYetFeatureUtils {
    public static String isItFriday(String today) {
        return "Friday".equals(today) ? "TGIF" : "Nope";
    }
}